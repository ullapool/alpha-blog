class Worklog < ApplicationRecord
    belongs_to :user
    validates :Beschreibung, presence: true, length: { minimum: 3, maximum: 100 }
    validates :Anzahl_Arbeiter, presence: true, length: { minimum: 1, maximum: 4 }
    validates :Stunden, presence: true, length: { minimum: 1, maximum: 300 }

end
