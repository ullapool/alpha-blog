require "test_helper"

class WorklogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @worklog = worklogs(:one)
  end

  test "should get index" do
    get worklogs_url
    assert_response :success
  end

  test "should get new" do
    get new_worklog_url
    assert_response :success
  end

  test "should create worklog" do
    assert_difference('Worklog.count') do
      post worklogs_url, params: { worklog: { Anzahl_Arbeiter: @worklog.Anzahl_Arbeiter, Arbeiter_01: @worklog.Arbeiter_01, Arbeiter_02: @worklog.Arbeiter_02, Arbeiter_03: @worklog.Arbeiter_03, Arbeiter_04: @worklog.Arbeiter_04, Beschreibung: @worklog.Beschreibung, Datum: @worklog.Datum, Stunden: @worklog.Stunden } }
    end

    assert_redirected_to worklog_url(Worklog.last)
  end

  test "should show worklog" do
    get worklog_url(@worklog)
    assert_response :success
  end

  test "should get edit" do
    get edit_worklog_url(@worklog)
    assert_response :success
  end

  test "should update worklog" do
    patch worklog_url(@worklog), params: { worklog: { Anzahl_Arbeiter: @worklog.Anzahl_Arbeiter, Arbeiter_01: @worklog.Arbeiter_01, Arbeiter_02: @worklog.Arbeiter_02, Arbeiter_03: @worklog.Arbeiter_03, Arbeiter_04: @worklog.Arbeiter_04, Beschreibung: @worklog.Beschreibung, Datum: @worklog.Datum, Stunden: @worklog.Stunden } }
    assert_redirected_to worklog_url(@worklog)
  end

  test "should destroy worklog" do
    assert_difference('Worklog.count', -1) do
      delete worklog_url(@worklog)
    end

    assert_redirected_to worklogs_url
  end
end
