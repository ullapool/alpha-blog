class CreateWorklogs < ActiveRecord::Migration[6.1]
  def change
    create_table :worklogs do |t|
      t.text :Beschreibung
      t.integer :Anzahl_Arbeiter
      t.string :Arbeiter_01
      t.string :Arbeiter_02
      t.string :Arbeiter_03
      t.string :Arbeiter_04
      t.integer :Stunden
      t.date :Datum

      t.timestamps
    end
  end
end
