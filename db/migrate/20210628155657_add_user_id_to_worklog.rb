class AddUserIdToWorklog < ActiveRecord::Migration[6.1]
  def change
    add_column :worklogs, :user_id, :int
  end
end
