Rails.application.routes.draw do
  
  
  root 'pages#home'
  resources :worklogs
  get 'about', to: 'pages#about'
  #get 'signup', to: 'users#new'
  resources :users, except: [:new]
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
end
